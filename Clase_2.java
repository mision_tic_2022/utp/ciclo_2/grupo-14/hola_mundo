import java.util.Scanner;

public class Clase_2 {
    public static void main(String[] args){
        String mensaje = "Hola mundo";
        String mayusculas = mensaje.toUpperCase();
        System.out.println(mayusculas);
        String minusculas = mensaje.toLowerCase();
        System.out.println(minusculas);
        //Capturar un caracter a partir de su posición
        char caracter = mensaje.charAt(0);
        System.out.println("Caracter en la posición cero-> "+caracter);
        //Llamar funciones
        libreria_matematica();
        calcular_numero_suerte();
    }

    public static void libreria_matematica(){
        //Raíz cuadrada
        double raiz_cuadrada = Math.sqrt(16);
        System.out.println("Raíz cuadrada de 16 es -> "+raiz_cuadrada);
        //Elevar a la potencia un número
        //Primer parámetro es la base y el segundo parámetro es la potencia
        double resp = Math.pow(5, 2);
        System.out.println("5^2 -> "+resp);
    }

    public static void calcular_numero_suerte(){
        //Crear variable de tipo Scanner
        Scanner leer = new Scanner(System.in);
        //Capturar el día de nacimiento
        System.out.print("Dia nacimiento: ");
        int dia = leer.nextInt();
        //Capturar el mes de nacimiento
        System.out.print("Mes nacimiento: ");
        int mes = leer.nextInt();
        //Capturar el año de nacimiento
        System.out.print("Anio nacimiento: ");
        int anio = leer.nextInt();
        //Sumatoria fecha nacimiento
        String suma_str = ""+(dia+mes+anio);
        //charAt(pos) -> retorna caracter en esa posición
        int numero_suerte = str_int(suma_str.charAt(0))  + str_int(suma_str.charAt(1)) +  str_int(suma_str.charAt(2)) +  str_int(suma_str.charAt(3));
        System.out.println("Número de la suerte-> "+numero_suerte);
    }

    public static int str_int(char caracter){
        return Integer.parseInt(""+caracter);
    }
}
