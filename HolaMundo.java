
// Hola mundo
public class HolaMundo{
    
    public static void main(String[] args){

        System.out.println("Hola mundo");
        System.out.print("Esto es otra linea");
        //Variables
        int numero = 5;
        double decimal = 10.2;
        boolean flag = false;
        String mensaje = "Hola mundo desde una variable";
        System.out.println("Número -> "+numero);
        System.out.println("Decimal -> "+decimal);
        System.out.println("Booleano ->"+flag);
        System.out.println(mensaje);
        //Operaciones Básicas
        int n1 = 10;
        int n2 = 15;
        int suma = n1+n2;
        suma += 10;
        int resta = n1-n2;
        int multiplicacion = n1*n2;
        double division = n1/n2;
        System.out.println("************************\nFUNCIONES\n*************************");
        //Llamado de funciones
        sumar();
        restar(10,2);
        multiplicacion = multiplicar();
        System.out.println("La multiplicación es: "+multiplicacion);

        division = dividir(20.5, 2.2);
        System.out.println("La división es: "+division);
        //Limitar cantidad de decimales
        System.out.printf("La división es: %.2f", division);
    }

    /***********
     * FUNCIONES
     *************/

    //Función sin parámetros y sin retorno
    public static void sumar(){
        int n1 = 15;
        int n2 = 20;
        int suma = n1+n2;
        System.out.println("La suma es: "+suma);
    }

    //Función con parámetros y sin retorno
    public static void restar(double n1, double n2){
        double resultado = n1-n2;
        System.out.println("La resta es: "+resultado);
    }

    //Función sin parámetros y con retorno
    public static int multiplicar(){
        int n1 = 10;
        int n2 = 5;
        return (n1*n2);
    }

    //Función con parámetros y con retorno
    public static double dividir(double n1, double n2){
        double resultado = n1/n2;
        return resultado;
    }
}